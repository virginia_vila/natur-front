import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  usuarioLogeado = false;

  constructor() { }

  ngOnInit(): void {
    this.existeSesion();
  }


  logout(){
    localStorage.clear();
    window.location.href = 'http://localhost:4200/';
  }

  existeSesion(){
    if(localStorage.getItem('auth_token') == null){
        this.usuarioLogeado = false;
    }else {
      this.usuarioLogeado = true;
    }
  }

}
