import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductosComponent } from './vistas/productos/productos.component'; 
import { LoginComponent } from './vistas/login/login.component';
import { RegistroComponent } from './vistas/registro/registro.component';
import { HomeComponent } from './vistas/home/home.component';
import { ProductoComponent } from 'src/app/vistas/producto/producto.component';
import { PerfilComponent } from './vistas/perfil/perfil.component';
import { HasRoleGuard } from './role.guard';
import { AgregarComponent } from './vistas/agregar/agregar.component';
import { EditarComponent } from './vistas/editar/editar.component';
import { ErrorComponent } from './plantillas/error/error.component';
import { CategoriasComponent } from './vistas/categorias/categorias.component';

const routes: Routes = [
  { path: '', component:HomeComponent},
  { path: 'login', component:LoginComponent},
  { path: 'registro', component:RegistroComponent},
  { path: '404', component:ErrorComponent},
  { path: 'agregar', component:AgregarComponent,  canActivate: [HasRoleGuard], data: {role: 'admin'}}, 
  { path: 'editar', component:EditarComponent, canActivate: [HasRoleGuard], data: {role: 'admin'}},
  { path: 'editar/:id', component:EditarComponent, canActivate: [HasRoleGuard], data: {role: 'admin'}},
  { path: 'categorias', component:CategoriasComponent, canActivate: [HasRoleGuard], data: {role: 'admin'}},
  { path: 'perfil', component:PerfilComponent},

  { path: 'producto/:id', component:ProductoComponent},
  { path: 'productos', component:ProductosComponent},
  { path: '**', redirectTo: '/404', pathMatch: 'full'}
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginComponent,HomeComponent,CategoriasComponent,ProductosComponent,RegistroComponent,ErrorComponent];
