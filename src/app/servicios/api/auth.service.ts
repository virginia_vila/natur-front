import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsuarioModel } from 'src/app/model/usuario.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseurl: string = "http://localhost:8080/";
  auth_token = localStorage.getItem("auth_token");
  user!:  UsuarioModel; 
  httpHeaders: HttpHeaders;


  constructor(private http: HttpClient) { 
    this.httpHeaders = new HttpHeaders({'Authorization': `Bearer ${this.auth_token}`});
  }


 addUser(usuario: any): Observable<any> {
    let direccion = this.baseurl + 'auth/register'
    return this.http.post<any>(direccion, usuario);      
  }

 loginUser(usuario: any): Observable<any> {
    let direccion = this.baseurl + 'auth/login'
    return this.http.post<any>(direccion, usuario);   

  }

 getUser(email: any): Observable<any> {
    let direccion = this.baseurl + `user/${email}`
    return this.http.get<any>(direccion, {headers: this.httpHeaders});      
  }

  getUser2(token: string): any{
    return JSON.parse(atob(token.split('.')[1]));
  }

 deleteUser(id:number): Observable<void> {
    let direccion = this.baseurl + "user/" + id;
    return this.http.delete<void>(direccion, {headers: this.httpHeaders});
  }

  getRoleByToken(token: string){
    if(token != null){      
       try{
       let tokenparse =  JSON.parse(atob(token.split('.')[1]));
       let role = tokenparse.role;       
       return role;
       } catch {
         console.log("Token incorrecto");
       }
    } 
  }


  tokenExpirado(error: number){
      if(error == 401 || this.auth_token == null){
        localStorage.clear(); 
         window.location.href = "http://localhost:4200/login";
      } 
  }


}
