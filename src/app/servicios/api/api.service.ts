import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Producto } from 'src/app/model/producto.model';
import { UsuarioModel } from 'src/app/model/usuario.model';
import { Categoria } from 'src/app/model/categoria.model';
import { Favorito } from 'src/app/model/favorito.model';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
   auth_token = localStorage.getItem("auth_token");
   baseurl: string = "http://localhost:8080/";
   httpHeaders: HttpHeaders;

    
    constructor(private http:HttpClient) { 
      this.httpHeaders = new HttpHeaders({'Authorization': `Bearer ${this.auth_token}`});
    }

    getProductos(): Observable<Producto[]> {
      let direccion = this.baseurl + "restnatur/";
      return this.http.get<Producto[]>(direccion);
    }

    getProducto(id:number): Observable<Producto[]> {
      let direccion = this.baseurl + "restnatur/productos/" + id;
      return this.http.get<Producto[]>(direccion);
    }

    addProduct(producto: Producto): Observable<Producto> {
      let direccion =  this.baseurl + "restnatur/add";
      return this.http.post<Producto>(direccion, producto, {headers: this.httpHeaders});            
    }

    deleteProducto(id:number): Observable<void> {
      let direccion = this.baseurl + "restnatur/delete/" + id;
      return this.http.delete<void>(direccion,{headers: this.httpHeaders});
    }

    updateProducto(producto: Producto): Observable<Producto> {
      let direccion = this.baseurl  + "restnatur/update/" + producto.id;
      return this.http.put<Producto>(direccion, producto, {headers: this.httpHeaders});
    }

    getCategorias(): Observable<Categoria[]> {
      let direccion = this.baseurl + "categorias";
      return this.http.get<Categoria[]>(direccion);
    }

    addCategoria(categoria: Categoria): Observable<Categoria> {
      let direccion = this.baseurl + "categorias/add";
      return this.http.post<Categoria>(direccion, categoria, {headers: this.httpHeaders});            
    }

    updateCategoria(categoria: Categoria): Observable<Categoria> {
      let direccion = this.baseurl + "categorias/update/" + categoria.id_categoria;
      return this.http.put<Categoria>(direccion, categoria, {headers: this.httpHeaders});
    }

    deleteCategoria(id:number): Observable<void> {
      let direccion =  this.baseurl + "categorias/delete/" + id;
      return this.http.delete<void>(direccion, {headers: this.httpHeaders});
    }

    getFavoritos(userid:number): Observable<Favorito[]> {
      let direccion = this.baseurl + "favoritos/" + userid;
      return this.http.get<Favorito[]>(direccion, {headers: this.httpHeaders});
    }

   deleteFavorito(id:number): Observable<void> {
      let direccion =  this.baseurl + "favoritos/delete/" + id;
      return this.http.delete<void>(direccion, {headers: this.httpHeaders});
    }

   addFavorito(favorito: Favorito): Observable<Favorito> {
      let direccion =  this.baseurl + "favoritos/add";
      return this.http.post<Favorito>(direccion, favorito, {headers: this.httpHeaders});            
    }    



}
