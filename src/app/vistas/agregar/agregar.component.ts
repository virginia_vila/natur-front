import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Categoria } from 'src/app/model/categoria.model';
import { Producto } from 'src/app/model/producto.model';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AuthService } from 'src/app/servicios/api/auth.service';
import { environment } from 'src/environments/environment.prod';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {

  productoCreado: Boolean = false;
  productoError: Boolean = false;
  tipos = ['Planta','Aceite'];
  categorias: Categoria[] = [];
  toFile: any;
  imgurl: string = '';
  spinner: boolean = false;

  constructor(private api: ApiService, private auth: AuthService) { }

  ngOnInit(): void {
    this.conseguirCategorias();
  }

  agregarProducto(agregarForm: NgForm): void {
    this.spinner = true;
    this.submit();

    setTimeout(() =>{
      this.api.addProduct(agregarForm.value).subscribe(
        (response: Producto) => {
          this.productoCreado = true;
          this.productoError = false;       
          agregarForm.reset();
          this.spinner = false;
        },
        (error: HttpErrorResponse) => {
          this.auth.tokenExpirado(error.status);
          this.productoCreado = false;
          this.productoError = true;
          agregarForm.reset();
          this.spinner = false;
        }
      );
    }, 3000);

    
  }


  conseguirCategorias(): void {
    this.api.getCategorias().subscribe(
      (response: Categoria[]) => {
        this.categorias = response;
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }



  subirArchivo(file: any) {
    const contentType = file.type;
    const bucket = new S3({
      accessKeyId: environment.AWS_ACCESS_KEY,
      secretAccessKey:  environment.AWS_SECRET_KEY,
      region: 'eu-central-1'});
    const params = {Bucket: 'naturbucket', Key: file.name, Body: file, ACL: 'public-read', ContentType: contentType };
    bucket.upload(params,  (err: any, data: any) => {
    if (err)  console.log('ERROR: ',JSON.stringify(err));
    else  this.imgurl = data.Location;});
    }
    

  submit() {
    const file = this.toFile.item(0);
    this.subirArchivo(file);
  
    }
    
  selectorImagen(event: any) {
    this.toFile = event.target.files;
    }



}
