import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Categoria } from 'src/app/model/categoria.model';
import { Producto } from 'src/app/model/producto.model';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AuthService } from 'src/app/servicios/api/auth.service';
import { environment } from 'src/environments/environment';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
  toFile: any;
  imgurl: string = '';
  producto:Producto[] = [];
  productoElegido:Producto;
  productoCambiado: Boolean = false;
  productoError: Boolean = false;
  id: number = 0;
  tipos = ['Planta','Aceite'];
  categorias: Categoria[] = [];


  constructor(private activatedRoute: ActivatedRoute, private api:ApiService, private auth:AuthService) {

    this.activatedRoute.params.subscribe( params => {
      this.id = params['id'];
      console.log(this.id);
     })
   }

  ngOnInit(): void {
    this.conseguirProducto(this.id);
    this.conseguirCategorias();
  }

  conseguirCategorias(): void {
    this.api.getCategorias().subscribe(
      (response: Categoria[]) => {
        this.categorias = response;
        console.log(this.categorias); 
      },(error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  
  editarProducto(producto: Producto): void {
    this.api.updateProducto(producto).subscribe(
      (response:  Producto) => {
        console.log(response);
        this.productoCambiado = true;
        this.productoError = false;
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.auth.tokenExpirado(error.status);
        this.productoError = true;
        this.productoCambiado = false;
      }
    );
  }

  conseguirProducto(id: number) {
    this.api.getProducto(id).subscribe(
      (response: Producto[]) => {
        this.producto = response;
        this.productoElegido = this.producto[0];
        console.log(this.productoElegido);   
      },
      (error: HttpErrorResponse) => {
        if(error.status == 404) alert("Tiene que seleccionar un producto para editarlo");
        window.location.href = "http://localhost:4200/productos"
      }
    );
  }

  subirArchivo(file: any) {
    const contentType = file.type;
    const bucket = new S3({
      accessKeyId: environment.AWS_ACCESS_KEY,
      secretAccessKey:  environment.AWS_SECRET_KEY,
      region: 'eu-central-1'});
    const params = {Bucket: 'naturbucket', Key: file.name, Body: file, ACL: 'public-read', ContentType: contentType };
    bucket.upload(params,  (err: any, data: any) => {
    if (err)  console.log('EROOR: ',JSON.stringify(err));
    else  this.productoElegido.imagen = data.Location;});
    }
    

  submit() {
    const file = this.toFile.item(0);
    this.subirArchivo(file);
  
    }
    
  selectorImagen(event: any) {
    this.toFile = event.target.files;
    this.submit();
    }

}





