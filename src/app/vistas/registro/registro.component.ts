import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/servicios/api/auth.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

    error: string;
    estado: boolean = false;
    registroForm = new FormGroup({
        nombre: new FormControl('',Validators.required),
        email: new FormControl('',Validators.required),
        password: new FormControl('',Validators.required),
    })

  constructor(private auth:AuthService) { }

  ngOnInit(): void {
  } 

  registrarUsuario(form: any): void {
    this.auth.addUser(form).subscribe(
      (response) => {
        localStorage.setItem('auth_token', response.access_token);
        localStorage.setItem('email', form.email);
        window.location.href = 'http://localhost:4200/perfil';
      },
      (err) => {
        console.log(err.error);
        if(err.error.email != null ) this.error = err.error.email;
        else  this.error = err.error.password;
        this.estado = true;
      }
    );
  }

  
}
