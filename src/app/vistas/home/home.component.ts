import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  mostrarRegistro = true;

  constructor() { }

  ngOnInit(): void {
    this.existeSesion();
  }

  existeSesion(){
    if(localStorage.getItem('auth_token') == null){
        this.mostrarRegistro = true;
    }else {
      this. mostrarRegistro = false;
    }
  }

  coco = false;
  aceite = false; 
  aloe = true;
  
  cambiarColor(color: string) {
      if(color == 'coco'){
        this.coco = true;
        this.aceite=false;
        this.aloe = false;
      }
      if(color == 'aceite'){
        this.coco = false;
        this.aceite = true;
        this.aloe = false;
      }
      if(color == 'aloe'){
        this.aloe = true;
        this.coco = false;
        this.aceite = false;
      }
  }

  


}
