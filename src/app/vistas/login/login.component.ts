import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioModel } from 'src/app/model/usuario.model';
import { AuthService } from 'src/app/servicios/api/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error: string;
  estado: boolean = false;
  user:  UsuarioModel;
  auth_token = localStorage.getItem("auth_token");

  loginForm = new FormGroup({
       email: new FormControl('',Validators.required),
       password: new FormControl('',Validators.required)
  })

  constructor(private auth:AuthService, private route: Router) { 

  }

  ngOnInit(): void {
  }



  onLoginUser(form: any): void {
    this.auth.loginUser(form).subscribe(
      (response) => {
        localStorage.setItem('auth_token', response.access_token);
        localStorage.setItem('email', form.email);        
        this.user = this.auth.getUser2(response.access_token); 
        window.location.href = 'http://localhost:4200/perfil';
       },
      (err) => {
        console.log(err.error);
        if(err.error.email != null ) this.error = err.error.email;
        else  this.error = err.error.password;
        this.estado = true;
      }
    );
  }

}
