import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Categoria } from 'src/app/model/categoria.model';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AuthService } from 'src/app/servicios/api/auth.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  categoriaCreado: Boolean = false;
  categoriaError: Boolean = false;

  constructor(private api: ApiService, private auth: AuthService) { }

  ngOnInit(): void {
  }

  agregarCategoria(addForm: NgForm): void {
    this.api.addCategoria(addForm.value).subscribe(
      (response: Categoria) => {
        console.log(response);
        this.categoriaCreado = true;
        this.categoriaError = false;
      },
      (error: HttpErrorResponse) => {
        this.auth.tokenExpirado(error.status);
        this.categoriaCreado = false;
        this.categoriaError = true;
      }
    );
  }

}
