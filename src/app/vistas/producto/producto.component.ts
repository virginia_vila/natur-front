import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/servicios/api/api.service';
import { ProductosComponent } from '../productos/productos.component';
import { Producto} from 'src/app/model/producto.model';
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Favorito } from 'src/app/model/favorito.model';
import { UsuarioModel } from 'src/app/model/usuario.model';
import { AuthService } from 'src/app/servicios/api/auth.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

   producto:Producto[] = [];
   productoElegido:Producto;
   id: number = 0;
   userid: any = localStorage.getItem('id');
   auth_token = localStorage.getItem("auth_token");
   role: string;
   logeado: boolean;
   favoritos: Favorito[] = [];
   productoEsFavorito: boolean = false;
   mostrarBotonFavorito: boolean = false;
   mostrarEsFavorito: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private api: ApiService, private auth: AuthService) { 
      this.role = this.auth.getRoleByToken(this.auth_token!);

       this.activatedRoute.params.subscribe( params => {
        this.id = params['id'];
        console.log(this.id);
       })
  }

  ngOnInit(): void {

    this.conseguirProducto(this.id);
    this.usuarioLogeado();
    if(this.role == 'guest') this.esFavorito(this.userid);

  }


  conseguirProducto(id: number) {
    this.api.getProducto(id).subscribe(
      (response: Producto[]) => {
        this.producto = response;
        this.productoElegido = this.producto[0];
        console.log(this.productoElegido);   

      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  agregarFavorito(agregarForm: NgForm): void {
    console.log(agregarForm);
  
        this.api.addFavorito(agregarForm.value).subscribe(
          (response: Favorito) => {
            console.log(response);
            this.mostrarBotonFavorito = false;
            this.productoEsFavorito = true;

          },
          (error: HttpErrorResponse) => {
            console.log(error);
            this.auth.tokenExpirado(error.status);
          }
        );


  }

  usuarioLogeado(){
   if (this.auth_token != null ){
     this.logeado = true;
       }

}

 esFavorito(userid:number){
  this.api.getFavoritos(userid).subscribe((response: Favorito[]) => { 
    this.favoritos = response;  
    let index = this.favoritos.findIndex(favorito => favorito.userid == this.userid && favorito.productoid == this.id);
    if(index == -1) {
      this.mostrarBotonFavorito = true;      
    } else {
      this.mostrarEsFavorito = true;
    }
  
  })
 }


}