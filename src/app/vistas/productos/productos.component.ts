import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/servicios/api/api.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Producto } from 'src/app/model/producto.model';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/servicios/api/auth.service';
import { Categoria } from 'src/app/model/categoria.model';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})

export class ProductosComponent implements OnInit {

   productos: Producto[] = [];
   categorias: Categoria[] = [];
   categoriaSeleccionada: number = 0;
   aux!:  Producto[];
   producto: any;
   error: boolean = false;
   errormsg: string= '';
   deleteProducto: Producto; 
   editProducto: Producto; 
   role: string;
   auth_token: string | null = localStorage.getItem("auth_token"); 
   spinner: boolean = true;


  constructor(private api:ApiService, private router:Router, private auth: AuthService) {
    this.role = this.auth.getRoleByToken(this.auth_token!);
  }
  
  ngOnInit(): void {
    this.conseguirProductos();
    this.conseguirCategorias();
  }

 verProducto(id:number){
    this.router.navigate( ['/producto/',id]);
  }

  conseguirProductos(): void {
    
    this.api.getProductos().subscribe(
      (response: Producto[]) => {
        this.productos = response;
        this.aux = response;
        this.spinner = false;
      },
      (error: HttpErrorResponse) => {
        this.error = true;
        this.errormsg = error.message;
        this.spinner = false;
      }
    );
  }

  conseguirCategorias(): void {
    this.api.getCategorias().subscribe(
      (response: Categoria[]) => {
        this.categorias = response;
        console.log(this.categorias);   
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }

  conseguirProducto(id:number){
    return this.productos[id];
  }

  buscarProducto(nombre: string): void {
    const results: Producto[] = [];
    for (const producto of this.aux) {
      if (producto.nombre.toLowerCase().indexOf(nombre.toLowerCase()) !== -1) {
        results.push(producto);
      }
    }
    this.aux = results;
    if (results.length === 0 || !nombre) {
      this.conseguirProductos();
    }
  }



  abrirModal(producto: Producto, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'delete') {
      this.deleteProducto = producto;
      button.setAttribute('data-target', '#deleteProductoModal');
    }
    container?.appendChild(button);
    button.click();
  }



  eliminarProducto(id:number) {
		this.api.deleteProducto(id).subscribe(data => {
      console.log(data);
      this.conseguirProductos();
    }, (error: HttpErrorResponse) => {
       this.auth.tokenExpirado(error.status);
    }
    );
	}

  editarProducto(id:number){
    this.router.navigate( ['/editar/',id]);
  }



  buscarCategoria(): void {

    let results: Producto[] = [];
    console.log(this.categoriaSeleccionada);
    
    for (const producto of this.productos) {
      if (producto.id_categ === this.categoriaSeleccionada) {
        results.push(producto);        
      }      
    }
    this.aux = results;

    if(this.categoriaSeleccionada == 0 ){
    this.conseguirProductos();
    }

  }








}


