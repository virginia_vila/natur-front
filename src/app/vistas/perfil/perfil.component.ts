import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/model/producto.model';
import { UsuarioModel } from 'src/app/model/usuario.model';
import { ApiService } from 'src/app/servicios/api/api.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/servicios/api/auth.service';
import { Categoria } from 'src/app/model/categoria.model';
import { Favorito } from 'src/app/model/favorito.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  productos: Producto[] = [];
  categorias: Categoria[] = [];
  favoritos: Favorito[] = [];
  usuarios: UsuarioModel[] = [];
  deleteProducto: Producto; 
  deleteCategoria: Categoria; 
  deleteUsuario: UsuarioModel; 
  editProducto: Producto; 
  editCategoria: Categoria;
  auth_token: string | null = localStorage.getItem("auth_token"); 
  usuario: UsuarioModel;
  usuariologin = false;
  role: string;
  usuarioarray: [] = [];
  errorCategoria: boolean = false;

  constructor(private auth: AuthService, private api:ApiService, private router:Router) {
    this.role = this.auth.getRoleByToken(this.auth_token!);
   }


  ngOnInit(): void {
    this.datosUsuario();
    this.conseguirCategorias();
  }


  conseguirCategorias(): void {
    this.api.getCategorias().subscribe(
      (response: Categoria[]) => {
        this.categorias = response;
        console.log(this.categorias);   
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }

  conseguirFavoritos(userid: number): void {
    this.api.getFavoritos(userid).subscribe(
      (response: Favorito[]) => {
        this.favoritos = response;
        console.log(this.favoritos);   
      },
      (error: HttpErrorResponse) => {
        console.log(error);
        this.auth.tokenExpirado(error.status);
      }
    );
  }



 eliminarFavorito(id:number) {
		this.api.deleteFavorito(id).subscribe(data => {
         this.conseguirFavoritos(this.usuario.id);
      }, (error: HttpErrorResponse) => {
         console.log(error);
         this.auth.tokenExpirado(error.status);
      }
    );
	}


 eliminarCuenta(id:number) {
		this.auth.deleteUser(id).subscribe(data => {
      console.log(data);
      window.location.href = "http://localhost:4200/login";
      localStorage.clear();
      }, (error: HttpErrorResponse) => {
      this.auth.tokenExpirado(error.status);
      }
    );
	}

  eliminarCategoria(id:number) {
		this.api.deleteCategoria(id).subscribe(data => {
      console.log(data);
      this.conseguirCategorias();
    }, (error: HttpErrorResponse) => {
       this.auth.tokenExpirado(error.status);
       if(error.error.message.includes("Cannot delete or update a parent row: a foreign key constraint fails")){
         this.errorCategoria = true;
       }
      }
    );
	}

  editarCategoria(categoria:Categoria): void {
    this.api.updateCategoria(categoria).subscribe(
      (response:  Categoria) => {
        console.log(response);
        this.conseguirCategorias();

      },
      (error: HttpErrorResponse) => {
        this.auth.tokenExpirado(error.status);
        console.log(error.message);

      }
    );
  }




  abrirModalUsuario(usuario: UsuarioModel, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'delete') {
      this.deleteUsuario = usuario;
      button.setAttribute('data-target', '#deleteUsuarioModal');
    }
    container?.appendChild(button);
    button.click();
  }

  abrirModalCategoria(categoria: Categoria, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'delete') {
      this.deleteCategoria = categoria;
      button.setAttribute('data-target', '#deleteCategoriaModal');
    }
    if (mode === 'edit') {
      this.editCategoria = categoria;
      button.setAttribute('data-target', '#updateCategoriaModal');
    }
    container?.appendChild(button);
    button.click();
  }


  datosUsuario(): void {
    let email = localStorage.getItem('email');
    this.auth.getUser(email).subscribe(
      (response: UsuarioModel) => {
        this.usuario = response;
        this.usuariologin = true;
        this.conseguirFavoritos(this.usuario.id);
        localStorage.setItem('id', this.usuario.id.toString());
        
      },
      (error: HttpErrorResponse) => {
        this.auth.tokenExpirado(error.status);
        this.usuariologin = false;
        
      }
    );
  }


  
  


}
