export interface Producto{
    id: number,
    nombre: string,
    descripcion: string,
    imagen: string,
    tipo_producto: string,
    url: string,
    id_categ: number,
    nombre_categ: string
}