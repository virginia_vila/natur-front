export interface UsuarioModel{
    id: number,
    email: string,
    password: string,
    nombre: string,
    role: string
}