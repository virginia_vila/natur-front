export interface Favorito{
    id: number,
    userid: number,
    productoid: number,
    nombreproducto: string,
    imagenproducto: string
}