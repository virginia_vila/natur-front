import { Injectable, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterLink, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UsuarioModel } from './model/usuario.model';
import { AuthService } from './servicios/api/auth.service';
import { LoginComponent } from './vistas/login/login.component';

@Injectable({
  providedIn: 'root'
})
export class HasRoleGuard implements CanActivate{

  auth_token: string | null = localStorage.getItem("auth_token"); 
  user:  UsuarioModel;
  role: string;

  constructor(private auth: AuthService){
    this.role = this.auth.getRoleByToken(this.auth_token!);
  }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.role == null ){alert('No estás logueado'); window.location.href = 'http://localhost:4200/login';}
    const isAdmin = this.role.includes(route.data['role']);

    if(!isAdmin){
      alert('No tienes permisos de administrador');
      window.location.href = 'http://localhost:4200/perfil';
    }

    return isAdmin;

  }
  
}
