import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './plantillas/header/header.component';
import { FooterComponent } from './plantillas/footer/footer.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { ProductosComponent } from './vistas/productos/productos.component';
import { RegistroComponent } from './vistas/registro/registro.component';
import { ProductoComponent } from './vistas/producto/producto.component';
import { PerfilComponent } from './vistas/perfil/perfil.component';
import { AuthService } from './servicios/api/auth.service';
import { LoginComponent } from './vistas/login/login.component';
import { EditarComponent } from './vistas/editar/editar.component';
import { AgregarComponent } from './vistas/agregar/agregar.component';
import { ErrorComponent } from './plantillas/error/error.component';
import { CategoriasComponent } from './vistas/categorias/categorias.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    routingComponents,
    ProductosComponent,
    RegistroComponent,
    ProductoComponent,
    PerfilComponent,
    EditarComponent,
    AgregarComponent,
    ErrorComponent,
    CategoriasComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService,LoginComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
